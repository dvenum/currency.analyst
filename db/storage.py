'''
    google firestore wrapper

    its very simple, but can be extented as well on this separate module
'''

from google.cloud import firestore

import logs

logger = logs.get_logger(__name__)


def save_pairs(pairs):
    '''
    '''
    if not pairs:
        logger.warning('save_pairs(). Empty dataset given')
        return None

    db = firestore.Client()
    doc = db.collection('bloomberg').document('currency')
    doc.set( {pair.desc:pair.as_dict() for pair in pairs} )

    return True

