#!/usr/bin/env python3

'''
    db objects

    We use firestore as storage and don't need django models for it
    So, it simple wrappers for values here

'''
BLOOMBERG_QUOTE_LINK = 'https://www.bloomberg.com/quote/'

class CurrencyPair(object):
    ''' one pair like USD-CAD
    '''
    def __init__(self, key, value):
        try:
            self.desc = key.strip().upper()
        except ValueError:
            self.desc = None

        if self.desc.count('-') == 1:
            self.left, self.right = self.desc.split('-')

        try:
            self.value = float(value)
        except ValueError:
            self.value = None

    def validate(self):
        return self.desc and self.value

    @property
    def url(self):
        return f'{BLOOMBERG_QUOTE_LINK}{self.left}{self.right}:CUR'

    @property
    def back_url(self):
        return f'{BLOOMBERG_QUOTE_LINK}{self.right}{self.left}:CUR'

    def as_dict(self):
        return {
            'desc': self.desc,
            'left': self.left,
            'right': self.right,
            'value': self.value,
        }

    def __repr__(self):
        if self.validate():
            return f'{self.desc}: {self.value}'
        return 'None'


if __name__=='__main__':
    pairs = []

    pair1 = CurrencyPair('usd-cad', '1.4')
    pair2 = CurrencyPair('cad-usd', '0.74')
    pairs.append(pair1)
    pairs.append(pair2)
    print( {pair.desc:pair.as_dict() for pair in pairs} )


    #print(pair.as_dict())

