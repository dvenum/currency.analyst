# currency.analyst

VMS Software test case

# Description

telegram bot that replies with current exchange rate  
message format:  
	amount currency_from currency_to  
  

query example:  
	10 usd rub  
  
query responce:  
	10 usd = 717.212  
  
use bloomberg to get rates (https://www.bloomberg.com/quote/USDRUB:CUR)  
  
bonus points:  
	* attach a picture with a graph with currecy fluctuation for the current day.  
  

# Deployment

It has two parts: updater api, based on django+rest as AppEngine spot. And telegram bot, based on Cloud Functions.  
Use ./deploy.sh and bot/deploy.sh for it.  
  
## GCloud points

* Store telegram token to secrets manager as CURRENCY_ANALYST_TELEGRAM_TOKEN
* Allow AppEngine service account access to it
* Enable Auth shield for api point
* Enable error reporting API
* Enable Firestore API

