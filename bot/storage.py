'''
    Firestore wrapper

'''

from datetime import datetime
from google.cloud import firestore

UPDATE_INTERVAL = 1800 # in seconds


class Pairs(object):
    ''' It update values with UPDATE_INTERVAL on get() call
    '''

    def __init__(self):
        self.current_time = None

    def get(self, left, right):
        '''
            {'USD-PLN': {'value': 3.7795, 'right': 'PLN', 'left': 'USD', 'desc': 'USD-PLN'}, .. }
        '''
        if not self.current_time or (datetime.now()-self.current_time).seconds >= UPDATE_INTERVAL:
            self.update()
            self.current_time = datetime.now()

        key = f'{left.upper()}-{right.upper()}'
        return self.data.get(key, {}).get('value')


    def update(self):
        db = firestore.Client()
        doc = db.collection('bloomberg').document('currency')
        self.data = doc.get().to_dict()


