#!/usr/bin/env bash

# HINTS

# allow access to secret key from cloud function:
#gcloud secrets add-iam-policy-binding CURRENCY_ANALYST_TELEGRAM_TOKEN \
#    --role roles/secretmanager.secretAccessor \
#    --member serviceAccount:vms-software@appspot.gserviceaccount.com

gcloud functions deploy webhook --runtime python37 --trigger-http

