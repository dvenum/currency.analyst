'''
    Bot dispatcher

'''

import os
import re
import telegram

import storage

from google.cloud import secretmanager
from google.cloud import error_reporting
log_client = error_reporting.Client()


secrets = secretmanager.SecretManagerServiceClient()
TELEGRAM_TOKEN = secrets.access_secret_version(
        "projects/130291602106/secrets/CURRENCY_ANALYST_TELEGRAM_TOKEN/versions/1"
).payload.data.decode("utf-8")

bot = telegram.Bot(token=TELEGRAM_TOKEN)
pairs = storage.Pairs()

OUTPUT_PRECISION = 3

MSG_INCORRECT_INPUT = 'Wrong input. Example: "10 usd rur"'
MSG_UNKNOWN_PAIR = 'Unknown currency. Pairs from bloomberg allowed only'


INPUT_COMMANDS = [
    ('CONVERT_CURRENCY', r'(?P<amount>[\d.]+) (?P<from>\w+) (?P<to>\w+)'),
]

def parse_input(msg):
    ''' on the proof-of-concept stage we should keep it simple
        normal factory, when we know more about input/output format
    '''

    for cmd, pattern in INPUT_COMMANDS:
        matched = re.match(pattern, msg)
        if not matched:
            continue
        return (cmd, matched.groupdict())

    return None


def do_conversion(payload):
    ''' CONVERT_CURRENCY command
        payload ex.: {'amount': '10', 'from': 'usd', 'to': 'rur'}
    '''
    try:
        amount = float(payload.get('amount'))
        left = payload.get('from')
        right = payload.get('to')
    except:
        return None

    if not all([amount, left, right]):
        return None

    currency_value = pairs.get(left, right)
    if not currency_value:
        return MSG_UNKNOWN_PAIR

    converted_value = round(amount * currency_value, OUTPUT_PRECISION)
    return f'{amount} {left} = {converted_value}'


def webhook(request):
    ''' Telegram service entrypoint
    '''

    try:
        if request.method == "POST":

            # get input
            update = telegram.Update.de_json(request.get_json(force=True), bot)
            chat_id = update.message.chat.id
            message = update.message.text
            parsed = parse_input(message)

            # process input
            reply_message = None
            if parsed:
                cmd, payload = parsed
                if cmd == 'CONVERT_CURRENCY':
                    reply_message = do_conversion(payload)
                #elif

            # write output
            if not reply_message:
                reply_message = MSG_INCORRECT_INPUT
            bot.sendMessage(chat_id=chat_id, text=reply_message)

        # WARN: 'ok' remove message from queue
        return "ok"

    except:
        log_client.report_exception()


