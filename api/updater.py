#!/usr/bin/env python3
"""
    cron entrypoint.
    It take actual currency values from bloomberg and preserve it in firestore db

"""
import consts
import logs
from api import bloomberg
from db import models
from db import storage

import requests
import time
from lxml import html
from collections import OrderedDict

logger = logs.get_logger(__name__)

COOKIE_URLS = ["https://www.bloomberg.com", "https://www.bloomberg.com/markets/currencies/"]
CATALOGUE = [
    "https://www.bloomberg.com/markets/currencies/americas",
    "https://www.bloomberg.com/markets/currencies/europe-africa-middle-east",
    "https://www.bloomberg.com/markets/currencies/asia-pacific",
]


def build_header():
    ''' headers is important or we see captcha request
    '''

    return OrderedDict([
        ('accept', 'application/json, text/javascript, */*; q=0.01'),
        ('accept-encoding', 'gzip, deflate, br'),
        ('accept-language', 'en-US,en;q=0.9,ru;q=0.8'),
        ('dnt', '1'),
        ('if-none-match', 'W/"Item4oezcqJ1zsmFMlqlcA=="'),
        ('referer', 'https://www.bloomberg.com/markets/currencies/'),
        ('sec-fetch-dest', 'empty'),
        ('sec-fetch-mode', 'cors'),
        ('sec-fetch-site', 'same-origin'),
        ('user-agent', consts.USER_AGENT),
    ])


def insistent_get(session, url):
    ''' bloomberg give 500 response occasionally, so we should repeat request
    '''

    retries_left = consts.MAX_RETRIES
    response = session.get(url)

    while response.status_code != 200 and retries_left > 0:
        time.sleep(consts.RETRY_TIMEOUT)
        retries_left -= 1
        response = session.get(url)

    return response if response.status_code==200 else None


def get_back_pairs(session, pairs):
    ''' Actual back value is not the same, as simple 1/$value, so we need to get it
        On bloombergs currency catalogue, we see only list of direct pairs
    '''

    back_pairs = []

    for pair in pairs:
        response = insistent_get(session, pair.back_url)
        if not response:
            logger.warning(f'couldn\'t get {pair.back_url}')
            continue

        ltree = html.fromstring(response.content)
        spans = ltree.xpath('//span[contains(@class, "priceText_")]')
        if not spans or len(spans)!=1:
            logger.error(f'couldn\'t parse {pair.back_url} content')
            continue
        
        back_key = f'{pair.right}-{pair.left}'
        back_value = spans[0].text
        back_pair = models.CurrencyPair(back_key, back_value)
        if back_pair.validate():
            back_pairs.append(back_pair)
            logger.debug(f'updated {back_pair}')
        else:
            logger.info(f'{back_key} has no value')

    return back_pairs


def update_currency_list():
    ''' take list of pairs, that bloomberg know about
    '''

    headers = build_header()
    session = requests.Session()
    session.headers.update(headers)
    for cu in COOKIE_URLS:
        _ = session.get(cu)

    pairs = []
    session.cookies.clear()
    for url in CATALOGUE:
        response = insistent_get(session, url)
        if not response:
            logger.warning(f'couldn\'t get {url}')
            continue

        ltree = html.fromstring(response.content)
        table = ltree.xpath('//table[@class="data-table"]')
        if table:
            for key, value in bloomberg.parse_currency_table(table[0]):
                pair = models.CurrencyPair(key, value)
                if pair.validate():
                    pairs.append(pair)
                    logger.debug(f'updated {pair}')
                else:
                    # normal behaviour, few pairs has '--' on bloomberg instead of value
                    logger.info(f'{key} has no value')
        else:
            logger.error('couldn\'t fetch {url}. more likely to captcha problem')
    
    pairs.extend(get_back_pairs(session, pairs))
    return pairs


def cron_job():
    ''' entrypoit of periodical task
    '''

    pairs = update_currency_list()
    storage.save_pairs(pairs)
    return 'done'


if __name__ == '__main__':
    pairs = update_currency_list()
    print(pairs)

