"""
    Set of web design related code
"""

def parse_currency_table(table):
    ''' 
        take currency name and value from "https://www.bloomberg.com/markets/currencies/americas"
    '''

    trs = table.xpath('.//tr[@class="data-table-row"]')
    for tr in trs:
        key = tr.xpath('./th/a/div[2]/text()')  # ' USD-CAD '
        value = tr.xpath('./td[1]/span[1]/text()')
        if key and value:
            # value may be '--', if not present, we check it later
            yield key[0].strip(), value[0].strip()

