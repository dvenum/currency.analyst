from django.urls import path
from api import views

from rest_framework import routers


router = routers.DefaultRouter()

urlpatterns = [
    path('update-currencies', views.update, name='update-currencies')
]

urlpatterns += router.urls
