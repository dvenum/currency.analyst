from rest_framework.response import Response
from rest_framework.decorators import api_view

from api import updater


@api_view()
def update(request):
    return Response(updater.cron_job())
